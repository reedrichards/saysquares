{ pkgs ? import <nixpkgs> {} }:

pkgs.mkShell {
  buildInputs = with pkgs.python3Packages; [
    python
    numpy
    scipy
    pandas
    matplotlib
    ipykernel
    seaborn 
    

    # pyttsx3
    heapdict
    chess

    pytest


  ];

  shellHook = ''
    export PYTHONPATH=$PYTHONPATH:.
  '';
}

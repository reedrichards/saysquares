#!/usr/bin/env python3
"""
Practice which color a square is light or dark
"""

from say_squares import SaySquare

s = SaySquare(
    config={
        # map keys to rank and files
        # 0 = file 1 and rank a
        # 1 is file 2 and rank b
        # and so on
        "keymap": {
            "a": 0,
            "b": 1,
            "c": 2,
            "d": 3,
            "e": 4,
            "f": 5,
            "g": 6,
            "h": 7,
            "1": 0,
            "2": 1,
            "3": 2,
            "4": 3,
            "5": 4,
            "6": 5,
            "7": 6,
            "8": 7,
            # "a": 'a',
            # "b": 'b',
            # "c": 'c',
            # "d": 'd',
            # "e": 'e',
            # "f": 'f',
            # "g": 'g',
            # "h": 'h',
        },
        # used for light square dark square
        "light_key": ";",
        "dark_key": "'",
    }
)

# s.which_color(5)
# s.train_valid_bishop_squares(5)
# s.train_valid_knight_squares(trials=5)
# s.train_move_bishop_from_to(trials=5)
s.train_move_knight_from_to(trials=5)
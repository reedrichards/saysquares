"""
"""

import datetime
import os
import random
import sqlite3
import time
from collections import ChainMap
from exceptions import InvalidInputLength, NullInput
from math import inf
from pathlib import Path
from typing import Optional
import platform

if platform.system() == "Linux":
    import pyttsx3
from chess import pgn
from heapdict import heapdict

class MacSpeechEngine:
    """
    MacSpeechEngine uses the built in mac speech engine
    """
    stuff_to_say = []

    def __init__(self, system = os.system ):
        self.system = system
        
    def say(self, text: str):
        """
        :param text:
        :return:
        """
        self.stuff_to_say.append(text)

    def runAndWait(self):
        """

        :return:
        """
        for text in self.stuff_to_say:
            self.system(f'say "{text}"')
        self.stuff_to_say = []



class Record:
    """
    Record answers to training sessions to a sqlite db
    """

    def __init__(
        self,
        db_file: str = "saysquare.db",
    ):
        """

        :param db_file:
        """
        self.db_file = db_file
        self.with_connection(do=self.initialize_tables)

    def initialize_tables(self, connection: sqlite3.Connection):
        """

        :return:
        """
        cur = connection.cursor()
        cur.execute(
            """CREATE TABLE IF NOT EXISTS light_dark (
            id INTEGER PRIMARY KEY AUTOINCREMENT,
            square TEXT,
            correct BOOLEAN,
            time_to_answer REAL,
            stamp TIMESTAMP
            );"""
        )
        cur.execute(
            """CREATE TABLE IF NOT EXISTS valid_bishop_squares (
            id INTEGER PRIMARY KEY AUTOINCREMENT,
            square TEXT,
            correct BOOLEAN,
            time_to_answer REAL,
            stamp TIMESTAMP,
            answer TEXT
            );"""
        )
        cur.execute(
            """CREATE TABLE IF NOT EXISTS valid_knight_squares (
            id INTEGER PRIMARY KEY AUTOINCREMENT,
            square TEXT,
            correct BOOLEAN,
            time_to_answer REAL,
            stamp TIMESTAMP,
            answer TEXT
            );"""
        )
        cur.execute(
            """CREATE TABLE IF NOT EXISTS move_knight (
            id INTEGER PRIMARY KEY AUTOINCREMENT,
            square TEXT,
            destination TEXT,
            correct BOOLEAN,
            time_to_answer REAL,
            stamp TIMESTAMP,
            answer TEXT,
            was_shortest_path BOOLEAN
            );"""
        )
        cur.execute(
            """CREATE TABLE IF NOT EXISTS move_bishop (
            id INTEGER PRIMARY KEY AUTOINCREMENT,
            square TEXT,
            destination TEXT,
            correct BOOLEAN,
            time_to_answer REAL,
            stamp TIMESTAMP,
            answer TEXT,
            was_shortest_path BOOLEAN
            );"""
        )

    def with_connection(self, do, *args, **kwargs):
        with sqlite3.connect(self.db_file) as con:
            do(connection=con, *args, **kwargs)

    def _add_light_or_dark_answer(
        self,
        connection: sqlite3.Connection,
        square: str,
        correct: bool,
        time_to_answer: float,
        stamp: datetime.datetime,
    ):
        cur = connection.cursor()
        cur.execute(
            """
        INSERT INTO light_dark (square, correct, time_to_answer, stamp) 
        VALUES (?, ?, ?, ?)
        """,
            (square, correct, time_to_answer, stamp),
        )

    def add_light_or_dark_answer(
        self,
        square: str,
        correct: bool,
        time_to_answer: float,
        stamp: datetime.datetime,
    ):
        self.with_connection(
            do=self._add_light_or_dark_answer,
            square=square,
            correct=correct,
            time_to_answer=time_to_answer,
            stamp=stamp,
        )

    def _add_valid_bishop_squares_answer(
        self,
        connection: sqlite3.Connection,
        square: str,
        correct: bool,
        time_to_answer: float,
        stamp: datetime.datetime,
        answer: str,
    ):
        cur = connection.cursor()
        cur.execute(
            """
        INSERT INTO valid_bishop_squares (square, correct, time_to_answer, stamp, answer) 
        VALUES (?, ?, ?, ?, ?)
        """,
            (square, correct, time_to_answer, stamp, answer),
        )

    def add_valid_bishop_squares_answer(
        self,
        square: str,
        correct: bool,
        time_to_answer: float,
        stamp: datetime.datetime,
        answer: str,
    ):
        self.with_connection(
            do=self._add_valid_bishop_squares_answer,
            square=square,
            correct=correct,
            time_to_answer=time_to_answer,
            stamp=stamp,
            answer=answer,
        )

    def _add_valid_knight_squares_answer(
        self,
        connection: sqlite3.Connection,
        square: str,
        correct: bool,
        time_to_answer: float,
        stamp: datetime.datetime,
        answer: str,
    ):
        cur = connection.cursor()
        cur.execute(
            """
        INSERT INTO valid_knight_squares (square, correct, time_to_answer, stamp, answer) 
        VALUES (?, ?, ?, ?, ?)
        """,
            (square, correct, time_to_answer, stamp, answer),
        )

    def add_valid_knight_squares_answer(
        self,
        square: str,
        correct: bool,
        time_to_answer: float,
        stamp: datetime.datetime,
        answer: str,
    ):
        self.with_connection(
            do=self._add_valid_knight_squares_answer,
            square=square,
            correct=correct,
            time_to_answer=time_to_answer,
            stamp=stamp,
            answer=answer,
        )

    def _add_move_knight_answer(
        self,
        connection: sqlite3.Connection,
        square: str,
        destination: str,
        correct: bool,
        time_to_answer: float,
        stamp: datetime.datetime,
        answer: str,
        was_shortest_path: bool,
    ):
        cur = connection.cursor()
        cur.execute(
            """
        INSERT INTO move_knight (square, destination, correct, time_to_answer, stamp, answer, was_shortest_path) 
        VALUES (:square, :destination, :correct, :time_to_answer, :stamp, :answer, :was_shortest_path)
        """,
            (
                square,
                destination,
                correct,
                time_to_answer,
                stamp,
                answer,
                was_shortest_path,
            ),
        )

    def add_move_knight_answer(
        self,
        square: str,
        destination: str,
        correct: bool,
        time_to_answer: float,
        stamp: datetime.datetime,
        answer: str,
        was_shortest_path: bool,
    ):
        self.with_connection(
            do=self._add_move_knight_answer,
            square=square,
            destination=destination,
            correct=correct,
            time_to_answer=time_to_answer,
            stamp=stamp,
            answer=answer,
            was_shortest_path=was_shortest_path,
        )

    def _add_move_bishop_answer(
        self,
        connection: sqlite3.Connection,
        square: str,
        destination: str,
        correct: bool,
        time_to_answer: float,
        stamp: datetime.datetime,
        answer: str,
        was_shortest_path: bool,
    ):
        cur = connection.cursor()
        cur.execute(
            """
        INSERT INTO move_bishop (square, destination, correct, time_to_answer, stamp, answer, was_shortest_path) 
        VALUES (:square, :destination, :correct, :time_to_answer, :stamp, :answer, :was_shortest_path)
        """,
            (
                square,
                destination,
                correct,
                time_to_answer,
                stamp,
                answer,
                was_shortest_path,
            ),
        )

    def add_move_bishop_answer(
        self,
        square: str,
        destination: str,
        correct: bool,
        time_to_answer: float,
        stamp: datetime.datetime,
        answer: str,
        was_shortest_path: bool,
    ):
        self.with_connection(
            do=self._add_move_bishop_answer,
            square=square,
            destination=destination,
            correct=correct,
            time_to_answer=time_to_answer,
            stamp=stamp,
            answer=answer,
            was_shortest_path=was_shortest_path,
        )


class SaySquare(object):
    """Utilities for practicing blind chess.

    Supports:
      [x] quizzing on color of squares
      [x] quizzing on valid squares of corner bishops
      [x] quizzing on valid squares of knight
      [x] moving pieces around the board and checking if taken shortest path
      [x] move bishop around board
      [x] behaviour configuration
      [ ] reporting on progress
      [ ] backup database to s3
      [ ] merging backups
      [ ] configure
      [ ] using spaced repetition to quiz on squares that are missed
      


    Attributes:
        random_square : returns random square from chess board as rank file tuple

    """

    files = ["a", "b", "c", "d", "e", "f", "g", "h"]
    files_index = {"a": 0, "b": 1, "c": 2, "d": 3, "e": 4, "f": 5, "g": 6, "h": 7}
    file_nato = {
        "a": "alpha",
        "b": "bravo",
        "c": "charlie",
        "d": "delta",
        "e": "echo",
        "f": "foxtrot",
        "g": "golf",
        "h": "hotel",
    }
    ranks = [str(n) for n in range(1, 9)]
    ranks_index = {"1": 0, "2": 1, "3": 2, "4": 3, "5": 4, "6": 5, "7": 6, "8": 7}

    defualt_config = {
        # map keys to rank and files
        # 0 = file 1 and rank a
        # 1 is file 2 and rank b
        # and so on
        "keymap": {"a": 0, "s": 1, "d": 2, "f": 3, "j": 4, "k": 5, "l": 6, ";": 7},
        # used for light square dark square
        "light_key": "f",
        "dark_key": "j",
        "db_file": "saysquares.db",
        "pgn_dir": "./pgns/",
    }

    def __init__(self, config: Optional[dict] = None):
        """

        :param config: override default config
        :returns:

        """
        if config:
            self.config = ChainMap(config, self.defualt_config)
        else:
            self.config = self.defualt_config

        # if linux
        if platform.system() == "Linux":
            engine = pyttsx3.init()
            engine.setProperty("volume", 1.0)

        else:
            engine = MacSpeechEngine()
        # setting up volume level  between 0 and 1
        self.engine = engine
        self.record = Record(db_file=self.config["db_file"])

    def random_square(self, same_color: Optional[tuple] = None):
        if same_color:
            # TODO: this is a really ineffecient way to do this
            choice = (random.choice(self.files), random.choice(self.ranks))
            while self.square_color(choice[0], choice[1]) != self.square_color(
                same_color[0], same_color[1]
            ):
                choice = (random.choice(self.files), random.choice(self.ranks))
            return choice
        return (random.choice(self.files), random.choice(self.ranks))

    def record_which_color_answer(
        self,
        square: str,
        correct: bool,
        time_to_answer: float,
        stamp: datetime.datetime,
    ):
        self.record.add_light_or_dark_answer(square, correct, time_to_answer, stamp)

    def which_color(self, trials=225):
        correct = 0
        incorrect = 0
        k = trials
        while k:
            self.engine.say("light or dark")
            self.engine.runAndWait()
            file_choice, rank_choice = self.random_square()
            self.engine.say(file_choice + rank_choice)
            self.engine.runAndWait()
            import os
            os.system('cls' if os.name == 'nt' else 'clear')
            print("square: ", file_choice + rank_choice)
            print("light: ", self.config["light_key"])
            print("dark: ", self.config["dark_key"])
            start = time.time()
            ans = input("color: ")
            end = time.time()
            taken = end - start
            if ans == self.get_color_as_key(file_choice, rank_choice):
                self.engine.say("correct")
                self.record_which_color_answer(
                    square=f"{file_choice}{rank_choice}",
                    correct=True,
                    time_to_answer=taken,
                    stamp=datetime.datetime.now(),
                )
                correct += 1
                print ("correct")
            else:
                self.engine.say("incorrect")
                self.record_which_color_answer(
                    square=f"{file_choice}{rank_choice}",
                    correct=False,
                    time_to_answer=taken,
                    stamp=datetime.datetime.now(),
                )
                incorrect += 1
                print ("incorrect")

            self.say_color(file_choice, rank_choice)
            self.engine.runAndWait()
            k -= 1
        print("correct: ", correct)
        print("incorrect: ", incorrect)
        print("trials: ", trials)

    def say_color(self, file_choice: str, rank_choice: str):
        """given a chess square, say its color

        Example::
            >>> s = SaySquare()
            >>> s.say_color("a", "1")
            "v"

        :para file_choice: string of file selected eg: "a"
        :param rank_choice: string of rank selected eg: "4"

        """
        if self.files.index(file_choice) % 2 == 0 and int(rank_choice) % 2 != 0:
            self.engine.say("dark")
        elif self.files.index(file_choice) % 2 != 0 and int(rank_choice) % 2 == 0:
            self.engine.say("dark")
        else:
            self.engine.say("light")
        self.engine.runAndWait()

    def get_color_as_key(self, file_choice: str, rank_choice: str) -> str:
        """given a chess square, represent it's color by the key
        denoted for its color in settings. used to compare a color to
        answer given in `self.which_color`

        Example::
            >>> s = SaySquare(config={"dark_key": "v"})
            >>> s.get_color_as_key("a", "1")
            "v"

        :para file_choice: string of file selected eg: "a"
        :param rank_choice: string of rank selected eg: "4"
        :returns: the key defined for the current color of the square eg: "j"

        """
        if self.files_index[file_choice] % 2 == 0 and int(rank_choice) % 2 != 0:
            return self.config["dark_key"]
        elif self.files_index[file_choice] % 2 != 0 and int(rank_choice) % 2 == 0:
            return self.config["dark_key"]
        else:
            return self.config["light_key"]

    def square_color(self, file_choice, rank_choice):
        if self.files_index[file_choice] % 2 == 0 and int(rank_choice) % 2 != 0:
            return "dark"
        elif self.files_index[file_choice] % 2 != 0 and int(rank_choice) % 2 == 0:
            return "dark"
        else:
            return "light"

    def squares_from_input(self, iput: str):
        """
        given a string from input, turn that string into a list of
        tuples in shape of (file, rank) where file and rank
        """
        if not iput:
            raise NullInput("no input given")
        if len(iput) == 0:
            raise InvalidInputLength("input of zero length given")
        if len(iput) % 2 != 0:
            raise InvalidInputLength(
                f"{iput} contains odd amount of characters, expected even for coordinates"
            )
        list_input = list(iput)

        squares = []
        while list_input:
            rank = list_input.pop()
            _file = list_input.pop()
            squares.append(
                (
                    self.files[self.config["keymap"][_file]],
                    self.ranks[self.config["keymap"][rank]],
                )
            )
        return squares

    def valid_bishop_squares(self, file_choice, rank_choice):
        file_index = self.files_index[file_choice]
        rank_index = self.ranks_index[rank_choice]

        valid_sqaures = []
        # north east squares
        i = file_index
        j = rank_index
        while i < 7 and j < 7:
            i += 1
            j += 1
            valid_sqaures.append((self.files[i], self.ranks[j]))
        # south east squares
        i = file_index
        j = rank_index
        while i < 7 and j > 0:
            i += 1
            j -= 1
            valid_sqaures.append((self.files[i], self.ranks[j]))
        # north west squares
        i = file_index
        j = rank_index
        while i > 0 and j < 7:
            i -= 1
            j += 1
            valid_sqaures.append((self.files[i], self.ranks[j]))
        # south west squares
        i = file_index
        j = rank_index
        while i > 0 and j > 0:
            i -= 1
            j -= 1
            valid_sqaures.append((self.files[i], self.ranks[j]))
        return valid_sqaures

    def record_valid_bishop_square_answer(
        self,
        square: str,
        correct: bool,
        time_to_answer: float,
        stamp: datetime.datetime,
        answer: str,
    ):
        self.record.add_valid_bishop_squares_answer(
            square, correct, time_to_answer, stamp, answer
        )

    def record_valid_knight_square_answer(
        self,
        square: str,
        correct: bool,
        time_to_answer: float,
        stamp: datetime.datetime,
        answer: str,
    ):
        self.record.add_valid_knight_squares_answer(
            square, correct, time_to_answer, stamp, answer
        )

    def train_valid_bishop_squares(self, pause_between=1, trials=7):
        self.engine.say("bishop squares")
        self.engine.runAndWait()
        k = trials
        total_incorrect = -1
        total_correct = 0
        while k:
            # TODO corner bishop?
            # f_choices = [self.files[0], self.files[-1]]
            # r_choices = [self.ranks[0], self.ranks[-1]]
            # f_c = random.choice(f_choices)
            # r_c = random.choice(r_choices)
            file_choice, rank_choice = self.random_square()
            valid_squares = self.valid_bishop_squares(file_choice, rank_choice)
            given = []
            incorrect = -1
            start = None
            while sorted(given) != sorted(valid_squares):
                self.engine.say(file_choice + rank_choice)
                self.engine.runAndWait()
                incorrect += 1
                if incorrect > 0:
                    self.record_valid_bishop_square_answer(
                        square=f"{file_choice}{rank_choice}",
                        correct=False,
                        time_to_answer=None,
                        stamp=datetime.datetime.now(),
                        answer="".join([f"{g[0]}{g[1]}" for g in given]),
                    )
                try:
                    if not start:
                        start = time.time()
                    print(f'{file_choice}{rank_choice}')
                    given = self.squares_from_input(input("squares: "))
                    end = time.time()
                    taken = end - start
                except InvalidInputLength as e:
                    self.engine.say(e)
                    self.engine.runAndWait()
                except NullInput:
                    continue
                except KeyError:
                    self.engine.say("input does not match keymap")
                    self.engine.runAndWait()

            self.engine.say("correct")
            self.engine.runAndWait()
            self.record_valid_bishop_square_answer(
                square=f"{file_choice}{rank_choice}",
                correct=True,
                time_to_answer=taken,
                stamp=datetime.datetime.now(),
                answer="".join([f"{g[0]}{g[1]}" for g in given]),
            )
            k -= 1
            total_incorrect += incorrect
            total_correct += 1
        print("correct: ", total_correct)
        print("incorrect: ", total_incorrect)
        print("trials: ", trials)

    def valid_knight_squares(self, _file, rank):
        """"""
        index_of_file = self.files_index[_file]
        index_of_rank = self.ranks_index[rank]
        possible_squares = [
            (index_of_file + 1, index_of_rank - 2),
            (index_of_file + 1, index_of_rank + 2),
            (index_of_file - 1, index_of_rank - 2),
            (index_of_file - 1, index_of_rank + 2),
            (index_of_file + 2, index_of_rank - 1),
            (index_of_file + 2, index_of_rank + 1),
            (index_of_file - 2, index_of_rank - 1),
            (index_of_file - 2, index_of_rank + 1),
        ]
        valid_squares = []
        while possible_squares:
            i, j = possible_squares.pop()
            if i < 0 or i > 7:
                continue
            elif j < 0 or j > 7:
                continue
            else:
                valid_squares.append((self.files[i], self.ranks[j]))
        return valid_squares

    def say_square_nato(self, file_choice, rank_choice):
        """"""
        f = self.file_nato[file_choice]
        self.engine.say(f"{f} {rank_choice}")
        self.engine.runAndWait()

    def train_valid_knight_squares(
        self,
        pause_between: int = 1,
        trials: int = 100,
    ):
        """TODO describe function"""
        self.engine.say("valid knight moves from")
        self.engine.runAndWait()
        k = trials
        total_incorrect = 0
        total_correct = 0
        while k:
            file_choice, rank_choice = self.random_square()
            valid_squares = self.valid_knight_squares(file_choice, rank_choice)
            given = []

            incorrect = -1
            start = None
            while sorted(given) != sorted(valid_squares):
                incorrect += 1
                if incorrect > 0:
                    self.record_valid_knight_square_answer(
                        square=f"{file_choice}{rank_choice}",
                        correct=False,
                        time_to_answer=None,
                        stamp=datetime.datetime.now(),
                        answer="".join([f"{g[0]}{g[1]}" for g in given]),
                    )
                self.say_square_nato(file_choice=file_choice, rank_choice=rank_choice)
                # self.engine.say(file_choice + rank_choice)
                # self.engine.runAndWait()
                try:
                    if not start:
                        start = time.time()
                    print(f'{file_choice}{rank_choice}')
                    given = self.squares_from_input(input("squares: "))
                    end = time.time()
                    taken = end - start
                except InvalidInputLength as e:
                    self.engine.say(e)
                    self.engine.runAndWait()
                except NullInput:
                    continue
                except KeyError:
                    self.engine.say("input does not match keymap")
                    self.engine.runAndWait()

            self.engine.say("correct")
            self.engine.runAndWait()
            self.record_valid_knight_square_answer(
                square=f"{file_choice}{rank_choice}",
                correct=False,
                time_to_answer=taken,
                stamp=datetime.datetime.now(),
                answer="".join([f"{g[0]}{g[1]}" for g in given]),
            )
            k -= 1
            total_incorrect += incorrect
            total_correct += 1
        print("correct: ", total_correct)
        print("incorrect: ", total_incorrect)
        print("trials: ", trials)

    def train_move_knight_from_to(self, trials=20):
        """
        picking a source and desination

        find shortest paths to destination using djikstra

          build graph where source.neighbors = self.valid_knight_squares(source)
          where neighbor not in seen

        get answer

        check if answer is valid path to destination

        check if answer is shortest path to destination


        """
        k = trials
        total_incorrect_paths = 0
        total_incorrect_shortest_paths = 0
        total_correct_paths = 0
        total_correct_shortest_paths = 0
        while k:
            source = self.random_square()
            destination = self.random_square()
            is_valid_path = False
            is_shortest_path = False
            incorrect_paths = -1
            incorrect_shortest_paths = -1
            while not (is_valid_path and is_shortest_path):
                self.engine.say("move knight from")
                self.engine.runAndWait()
                self.engine.say(source[0] + source[1])
                self.engine.runAndWait()
                self.engine.say(destination[0] + destination[1])
                self.engine.runAndWait()
                incorrect_paths += 1
                incorrect_shortest_paths += 1
                start = time.time()
                print(f'{source[0]}{source[1]}')
                print(f'{destination[0]}{destination[1]}')
                given = input("path: ")
                if not given:
                    continue
                end = time.time()
                taken = end - start
                is_valid_path = self.valid_knight_path(source, destination, given)
                if is_valid_path:
                    total_correct_paths += 1
                    distance = self.shortest_path_knight(source)
                    is_shortest_path = len(given) / 2 == distance[destination]
                self.record.add_move_knight_answer(
                    square=f"{source[0]}{source[1]}",
                    destination=f"{destination[0]}{destination[1]}",
                    correct=is_valid_path,
                    stamp=datetime.datetime.now(),
                    time_to_answer=taken,
                    was_shortest_path=is_shortest_path,
                    answer="".join(
                        [
                            f"{g[0]}{g[1]}"
                            for g in reversed(self.squares_from_input(given))
                        ]
                    ),
                )
            self.engine.say("correct")
            self.engine.runAndWait()
            k -= 1
            total_incorrect_paths += incorrect_paths
            total_incorrect_shortest_paths += incorrect_shortest_paths
            total_correct_shortest_paths += 1
        print("correct paths: ", total_correct_paths)
        print("correct shortest paths: ", total_correct_shortest_paths)
        print("incorrect paths: ", total_incorrect_paths)
        print("incorrect shortest paths: ", total_incorrect_shortest_paths)
        print("trials: ", trials)

    def train_move_bishop_from_to(self, trials=20):
        """
        picking a source and desination

        find shortest paths to destination using djikstra

          build graph where source.neighbors = self.valid_knight_squares(source)
          where neighbor not in seen

        get answer

        check if answer is valid path to destination

        check if answer is shortest path to destination


        """
        k = trials
        total_incorrect_paths = 0
        total_incorrect_shortest_paths = 0
        total_correct_paths = 0
        total_correct_shortest_paths = 0
        while k:
            source = self.random_square()  # TODO light or dark random
            # TODO light or dark random based on source
            destination = self.random_square(same_color=source)
            is_valid_path = False
            is_shortest_path = False
            incorrect_paths = -1
            incorrect_shortest_paths = -1
            while not (is_valid_path and is_shortest_path):
                self.engine.say("move bishop from")
                self.engine.runAndWait()
                self.engine.say(source[0] + source[1])
                self.engine.runAndWait()
                self.engine.say(destination[0] + destination[1])
                self.engine.runAndWait()
                incorrect_paths += 1
                incorrect_shortest_paths += 1
                start = time.time()
                print(f'{source[0]}{source[1]}')
                print(f'{destination[0]}{destination[1]}')
                given = input("path: ")
                if not given:
                    continue
                end = time.time()
                taken = end - start
                is_valid_path = self.valid_bishop_path(source, destination, given)
                if is_valid_path:
                    total_correct_paths += 1
                    distance = self.shortest_path_bishop(source)
                    is_shortest_path = len(given) / 2 == distance[destination]
                self.record.add_move_bishop_answer(
                    square=f"{source[0]}{source[1]}",
                    destination=f"{destination[0]}{destination[1]}",
                    correct=is_valid_path,
                    stamp=datetime.datetime.now(),
                    time_to_answer=taken,
                    was_shortest_path=is_shortest_path,
                    answer="".join(
                        [
                            f"{g[0]}{g[1]}"
                            for g in reversed(self.squares_from_input(given))
                        ]
                    ),
                )

            self.engine.say("correct")
            self.engine.runAndWait()
            k -= 1
            total_incorrect_paths += incorrect_paths
            total_incorrect_shortest_paths += incorrect_shortest_paths
            total_correct_shortest_paths += 1
        print("correct paths: ", total_correct_paths)
        print("correct shortest paths: ", total_correct_shortest_paths)
        print("incorrect paths: ", total_incorrect_paths)
        print("incorrect shortest paths: ", total_incorrect_shortest_paths)
        print("trials: ", trials)

    def valid_knight_path(self, source, destination, pathstring):
        if not pathstring:
            return False
        # if pathstring[:-2] != destination[0] + destination[1]:
        #     return False
        squares = self.squares_from_input(pathstring)

        next_squares = self.valid_knight_squares(source[0], source[1])
        current = source
        while squares:
            next_move = squares.pop()
            if (next_move[0], next_move[1]) not in next_squares:
                return False
            current = (next_move[0], next_move[1])
            next_squares = self.valid_knight_squares(current[0], current[1])

        return current == destination

    def valid_bishop_path(self, source, destination, pathstring):
        if not pathstring:
            return False
        # if pathstring[:-2] != destination[0] + destination[1]:
        #     return False
        squares = self.squares_from_input(pathstring)

        next_squares = self.valid_bishop_squares(source[0], source[1])
        current = source
        while squares:
            next_move = squares.pop()
            if (next_move[0], next_move[1]) not in next_squares:
                return False
            current = (next_move[0], next_move[1])
            next_squares = self.valid_bishop_squares(current[0], current[1])

        return current == destination

    def shortest_path_knight(self, source):
        """
        calculates shortest paths as a knight using dijkstra
        """
        all_squares = []
        for i in self.ranks:
            for j in self.files:
                all_squares.append((j, i))

        # build edges
        distance = {}
        distance[source] = 0
        Q = heapdict()
        for vertex in all_squares:
            if vertex != source:
                distance[vertex] = inf
            Q[vertex] = distance[vertex]
        while Q:
            u, p = Q.popitem()  # extract min
            for v in self.valid_knight_squares(u[0], u[1]):
                if distance[v] > distance[u] + 1:
                    distance[v] = distance[u] + 1
                    Q[v] = distance[v]
        return distance

    def shortest_path_bishop(self, source):
        all_squares = []
        for i in self.ranks:
            for j in self.files:
                all_squares.append((j, i))

        # build edges
        distance = {}
        distance[source] = 0
        Q = heapdict()
        for vertex in all_squares:
            if vertex != source:
                distance[vertex] = inf
            Q[vertex] = distance[vertex]
        while Q:
            u, p = Q.popitem()  # extract min
            for v in self.valid_bishop_squares(u[0], u[1]):
                if distance[v] > distance[u] + 1:
                    distance[v] = distance[u] + 1
                    Q[v] = distance[v]
        return distance

    def read_game(self, delay=None, print_board=False):
        pgn_dir = Path(self.config["pgn_dir"])
        for _file in os.listdir(pgn_dir):
            with open(Path(self.config["pgn_dir"] + _file)) as f:
                game = pgn.read_game(f)
            board = game.board()
            self.engine.say(game.headers["Event"])
            self.engine.runAndWait()
            counter = 0
            moves = [move for move in game.mainline_moves()]
            i = 0
            while i < len(moves):
                self.engine.say(moves[i].uci())
                self.engine.runAndWait()
                board.push(moves[i])
                if print_board:
                    print(board)
                i += 1
                if delay is not None:
                    time.sleep(delay)
                else:
                    # TODO: allow configuration for these
                    j = input("Press Enter to continue...")
                    if j:
                        # TODO: b10 would be back 10 moves
                        if j[0] == "b":  # back one move
                            i -= 2
                            board = game.board()
                            for k in range(i):
                                board.push(moves[k])
                        if j[0] == "r":  # repeat move
                            i -= 1
                            board = game.board()
                            for k in range(i):
                                board.push(moves[k])
                        if j[0] == "p":  # print board
                            print(board)


if __name__ == "__main__":
    s = SaySquare(
        config={
            # map keys to rank and files
            # 0 = file 1 and rank a
            # 1 is file 2 and rank b
            # and so on
            "keymap": {
                "a": 0,
                "b": 1,
                "c": 2,
                "d": 3,
                "e": 4,
                "f": 5,
                "g": 6,
                "h": 7,
                "1": 0,
                "2": 1,
                "3": 2,
                "4": 3,
                "5": 4,
                "6": 5,
                "7": 6,
                "8": 7,
            },
            # used for light square dark square
            "light_key": ";",
            "dark_key": "'",
        }
    )
    s.read_game(delay=10)

"""
Tests for main module
"""
import pytest  # NOQA: F401

from say_squares import SaySquare


class TestValidKnightPath:
    s = SaySquare(config={
        "db_file": ":memory:",
        "keymap": {
            "a": 0,
            "b": 1,
            "c": 2,
            "d": 3,
            "e": 4,
            "f": 5,
            "g": 6,
            "h": 7,
            "1": 0,
            "2": 1,
            "3": 2,
            "4": 3,
            "5": 4,
            "6": 5,
            "7": 6,
            "8": 7,
        },
    })

    @pytest.mark.parametrize(
        "source,destination,pathstring,expected",
        [[("a", "1"), ("b", "3"), "b3", True],
         [("a", "1"), ("c", "2"), "c2", True],
         [("h", "8"), ("h", "1"), "g6f5h3f2h1", False],
         [("h", "8"), ("h", "1"), "g6e5g4f2h1", True]
         ],
    )
    def test_valid_knight_path(self, source, destination, pathstring, expected):
        assert self.s.valid_knight_path(
            source, destination, pathstring) == expected


class TestShortestPathKnight:
    s = SaySquare(config={"db_file": ":memory:",})

    @pytest.mark.parametrize(
        "_file,rank,expected",
        [
            [
                "h",  # https://i.stack.imgur.com/UV7lZ.png
                "8",
                {
                    ("a", "1"): 6,
                    ("a", "2"): 5,
                    ("a", "3"): 4,
                    ("a", "4"): 5,
                    ("a", "5"): 4,
                    ("a", "6"): 5,
                    ("a", "7"): 4,
                    ("a", "8"): 5,
                    ("b", "1"): 5,
                    ("b", "2"): 4,
                    ("b", "3"): 5,
                    ("b", "4"): 4,
                    ("b", "5"): 3,
                    ("b", "6"): 4,
                    ("b", "7"): 3,
                    ("b", "8"): 4,
                    ("c", "1"): 4,
                    ("c", "2"): 5,
                    ("c", "3"): 4,
                    ("c", "4"): 3,
                    ("c", "5"): 4,
                    ("c", "6"): 3,
                    ("c", "7"): 4,
                    ("c", "8"): 3,
                    ("d", "1"): 5,
                    ("d", "2"): 4,
                    ("d", "3"): 3,
                    ("d", "4"): 4,
                    ("d", "5"): 3,
                    ("d", "6"): 2,
                    ("d", "7"): 3,
                    ("d", "8"): 2,
                    ("e", "1"): 4,
                    ("e", "2"): 3,
                    ("e", "3"): 4,
                    ("e", "4"): 3,
                    ("e", "5"): 2,
                    ("e", "6"): 3,
                    ("e", "7"): 2,
                    ("e", "8"): 3,
                    ("f", "1"): 5,
                    ("f", "2"): 4,
                    ("f", "3"): 3,
                    ("f", "4"): 2,
                    ("f", "5"): 3,
                    ("f", "6"): 4,
                    ("f", "7"): 1,
                    ("f", "8"): 2,
                    ("g", "1"): 4,
                    ("g", "2"): 3,
                    ("g", "3"): 4,
                    ("g", "4"): 3,
                    ("g", "5"): 2,
                    ("g", "6"): 1,
                    ("g", "7"): 4,
                    ("g", "8"): 3,
                    ("h", "1"): 5,
                    ("h", "2"): 4,
                    ("h", "3"): 3,
                    ("h", "4"): 2,
                    ("h", "5"): 3,
                    ("h", "6"): 2,
                    ("h", "7"): 3,
                    ("h", "8"): 0,
                },
            ],
        ],
    )
    def test_shortest_path_knight(self, _file, rank, expected):
        assert self.s.shortest_path_knight((_file, rank)) == expected


class TestValidBishopSquares:
    s = SaySquare(config={"db_file": ":memory:"})


    @pytest.mark.parametrize(
        "_file,rank,expected",
        [
            [
                "h",
                "1",
                [
                    ("a", "8"),
                    ("b", "7"),
                    ("c", "6"),
                    ("d", "5"),
                    ("e", "4"),
                    ("f", "3"),
                    ("g", "2"),
                ],
            ],
            [
                "h",
                "8",
                [
                    ("g", "7"),
                    ("f", "6"),
                    ("e", "5"),
                    ("d", "4"),
                    ("c", "3"),
                    ("b", "2"),
                    ("a", "1"),
                ],
            ],
            [
                "a",
                "2",
                [
                    ("b", "1"),
                    ("b", "3"),
                    ("c", "4"),
                    ("d", "5"),
                    ("e", "6"),
                    ("f", "7"),
                    ("g", "8"),
                ],
            ],
            [
                "a",
                "1",
                [
                    ("b", "2"),
                    ("c", "3"),
                    ("d", "4"),
                    ("e", "5"),
                    ("f", "6"),
                    ("g", "7"),
                    ("h", "8"),
                ],
            ],
            [
                "a",
                "8",
                [
                    ("b", "7"),
                    ("c", "6"),
                    ("d", "5"),
                    ("e", "4"),
                    ("f", "3"),
                    ("g", "2"),
                    ("h", "1"),
                ],
            ],
        ],
    )
    def test_valid_bisop_squares(self, _file, rank, expected):
        assert sorted(self.s.valid_bishop_squares(
            _file, rank)) == sorted(expected)


class TestValidKnightSquares:
    s = SaySquare(config={"db_file": ":memory:"})

    # TODO complete this table or generate it

    @pytest.mark.parametrize(
        "_file,rank,expected",
        [
            ["a", "1", [("b", "3"), ("c", "2")]],
            ["a", "2", [("b", "4"), ("c", "3"), ("c", "1")]],
            ["a", "3", [("b", "5"), ("c", "4"), ("c", "2"), ("b", "1")]],
            ["a", "4", [("b", "6"), ("c", "5"), ("c", "3"), ("b", "2")]],
            ["a", "5", [("b", "7"), ("c", "6"), ("c", "4"), ("b", "3")]],
            ["a", "6", [("b", "8"), ("c", "7"), ("c", "5"), ("b", "4")]],
            ["a", "7", [("c", "8"), ("c", "6"), ("b", "5")]],
            ["a", "8", [("c", "7"), ("b", "6")]],
            ["b", "1", [("a", "3"), ("c", "3"), ("d", "2")]],
            ["b", "2", [("a", "4"), ("c", "4"), ("d", "3"), ("d", "1")]],
        ],
    )
    def test_valid_kinght_squares(self, _file, rank, expected):
        assert sorted(self.s.valid_knight_squares(
            _file, rank)) == sorted(expected)


class TestSquaresFromInput:
    s = SaySquare(config={"db_file": ":memory:"})


    s_real_keys = SaySquare(
        config={
            # map keys to rank and files
            # 0 = file 1 and rank a
            # 1 is file 2 and rank b
            # and so on
            "db_file": ":memory:",
            "keymap": {
                "a": 0,
                "b": 1,
                "c": 2,
                "d": 3,
                "e": 4,
                "f": 5,
                "g": 6,
                "h": 7,
                "1": 0,
                "2": 1,
                "3": 2,
                "4": 3,
                "5": 4,
                "6": 5,
                "7": 6,
                "8": 7,
            },
            # used for light square dark square
            "light_key": ";",
            "dark_key": "'",
        }
    )

    @pytest.mark.parametrize(
        "iput,expected",
        [
            ["a1", [("a", "1")]],
            ["a2", [("a", "2")]],
            ["a3", [("a", "3")]],
            ["a4", [("a", "4")]],
            ["a5", [("a", "5")]],
            ["a6", [("a", "6")]],
            ["a7", [("a", "7")]],
            ["a8", [("a", "8")]],
            ["b1", [("b", "1")]],
            ["b2", [("b", "2")]],
            ["b3", [("b", "3")]],
            ["b4", [("b", "4")]],
            ["b5", [("b", "5")]],
            ["b6", [("b", "6")]],
            ["b7", [("b", "7")]],
            ["b8", [("b", "8")]],
            ["c1", [("c", "1")]],
            ["c2", [("c", "2")]],
            ["c3", [("c", "3")]],
            ["c4", [("c", "4")]],
            ["c5", [("c", "5")]],
            ["c6", [("c", "6")]],
            ["c7", [("c", "7")]],
            ["c8", [("c", "8")]],
            ["d1", [("d", "1")]],
            ["d2", [("d", "2")]],
            ["d3", [("d", "3")]],
            ["d4", [("d", "4")]],
            ["d5", [("d", "5")]],
            ["d6", [("d", "6")]],
            ["d7", [("d", "7")]],
            ["d8", [("d", "8")]],
            ["e1", [("e", "1")]],
            ["e2", [("e", "2")]],
            ["e3", [("e", "3")]],
            ["e4", [("e", "4")]],
            ["e5", [("e", "5")]],
            ["e6", [("e", "6")]],
            ["e7", [("e", "7")]],
            ["e8", [("e", "8")]],
            ["f1", [("f", "1")]],
            ["f2", [("f", "2")]],
            ["f3", [("f", "3")]],
            ["f4", [("f", "4")]],
            ["f5", [("f", "5")]],
            ["f6", [("f", "6")]],
            ["f7", [("f", "7")]],
            ["f8", [("f", "8")]],
            ["g1", [("g", "1")]],
            ["g2", [("g", "2")]],
            ["g3", [("g", "3")]],
            ["g4", [("g", "4")]],
            ["g5", [("g", "5")]],
            ["g6", [("g", "6")]],
            ["g7", [("g", "7")]],
            ["g8", [("g", "8")]],
            ["h1", [("h", "1")]],
            ["h2", [("h", "2")]],
            ["h3", [("h", "3")]],
            ["h4", [("h", "4")]],
            ["h5", [("h", "5")]],
            ["h6", [("h", "6")]],
            ["h7", [("h", "7")]],
            ["h8", [("h", "8")]],
            ["h8a8", [("h", "8"), ("a", "8")]],
        ],
    )
    def test_real_key_config(self, iput, expected):
        assert sorted(self.s_real_keys.squares_from_input(
            iput)) == sorted(expected)

    @pytest.mark.parametrize(
        "iput,expected",
        [
            ["aa", [("a", "1")]],
            ["as", [("a", "2")]],
            ["ad", [("a", "3")]],
            ["af", [("a", "4")]],
            ["aj", [("a", "5")]],
            ["ak", [("a", "6")]],
            ["al", [("a", "7")]],
            ["a;", [("a", "8")]],
            ["sa", [("b", "1")]],
            ["ss", [("b", "2")]],
            ["sd", [("b", "3")]],
            ["sf", [("b", "4")]],
            ["sj", [("b", "5")]],
            ["sk", [("b", "6")]],
            ["sl", [("b", "7")]],
            ["s;", [("b", "8")]],
            ["da", [("c", "1")]],
            ["ds", [("c", "2")]],
            ["dd", [("c", "3")]],
            ["df", [("c", "4")]],
            ["dj", [("c", "5")]],
            ["dk", [("c", "6")]],
            ["dl", [("c", "7")]],
            ["d;", [("c", "8")]],
            ["fa", [("d", "1")]],
            ["fs", [("d", "2")]],
            ["fd", [("d", "3")]],
            ["ff", [("d", "4")]],
            ["fj", [("d", "5")]],
            ["fk", [("d", "6")]],
            ["fl", [("d", "7")]],
            ["f;", [("d", "8")]],
            ["ja", [("e", "1")]],
            ["js", [("e", "2")]],
            ["jd", [("e", "3")]],
            ["jf", [("e", "4")]],
            ["jj", [("e", "5")]],
            ["jk", [("e", "6")]],
            ["jl", [("e", "7")]],
            ["j;", [("e", "8")]],
            ["ka", [("f", "1")]],
            ["ks", [("f", "2")]],
            ["kd", [("f", "3")]],
            ["kf", [("f", "4")]],
            ["kj", [("f", "5")]],
            ["kk", [("f", "6")]],
            ["kl", [("f", "7")]],
            ["k;", [("f", "8")]],
            ["la", [("g", "1")]],
            ["ls", [("g", "2")]],
            ["ld", [("g", "3")]],
            ["lf", [("g", "4")]],
            ["lj", [("g", "5")]],
            ["lk", [("g", "6")]],
            ["ll", [("g", "7")]],
            ["l;", [("g", "8")]],
            [";a", [("h", "1")]],
            [";s", [("h", "2")]],
            [";d", [("h", "3")]],
            [";f", [("h", "4")]],
            [";j", [("h", "5")]],
            [";k", [("h", "6")]],
            [";l", [("h", "7")]],
            [";;", [("h", "8")]],
        ],
    )
    def test_squares_from_input(self, iput, expected):
        assert self.s.squares_from_input(iput) == expected


class TestGetColorAsKey:
    s = SaySquare(config={"light_key": "0", "dark_key": "1", "db_file": ":memory:"})

    @pytest.mark.parametrize(
        "_file,rank,ans",
        [
            ["a", "1", "1"],
            ["a", "2", "0"],
            ["a", "3", "1"],
            ["a", "4", "0"],
            ["a", "5", "1"],
            ["a", "6", "0"],
            ["a", "7", "1"],
            ["a", "8", "0"],
            ["b", "1", "0"],
            ["b", "2", "1"],
            ["b", "3", "0"],
            ["b", "4", "1"],
            ["b", "5", "0"],
            ["b", "6", "1"],
            ["b", "7", "0"],
            ["b", "8", "1"],
            ["c", "1", "1"],
            ["c", "2", "0"],
            ["c", "3", "1"],
            ["c", "4", "0"],
            ["c", "5", "1"],
            ["c", "6", "0"],
            ["c", "7", "1"],
            ["c", "8", "0"],
            ["d", "1", "0"],
            ["d", "2", "1"],
            ["d", "3", "0"],
            ["d", "4", "1"],
            ["d", "5", "0"],
            ["d", "6", "1"],
            ["d", "7", "0"],
            ["d", "8", "1"],
            ["e", "1", "1"],
            ["e", "2", "0"],
            ["e", "3", "1"],
            ["e", "4", "0"],
            ["e", "5", "1"],
            ["e", "6", "0"],
            ["e", "7", "1"],
            ["e", "8", "0"],
            ["f", "1", "0"],
            ["f", "2", "1"],
            ["f", "3", "0"],
            ["f", "4", "1"],
            ["f", "5", "0"],
            ["f", "6", "1"],
            ["f", "7", "0"],
            ["f", "8", "1"],
            ["g", "1", "1"],
            ["g", "2", "0"],
            ["g", "3", "1"],
            ["g", "4", "0"],
            ["g", "5", "1"],
            ["g", "6", "0"],
            ["g", "7", "1"],
            ["g", "8", "0"],
            ["h", "1", "0"],
            ["h", "2", "1"],
            ["h", "3", "0"],
            ["h", "4", "1"],
            ["h", "5", "0"],
            ["h", "6", "1"],
            ["h", "7", "0"],
            ["h", "8", "1"],
        ],
    )
    def test_get_color_as_key(self, _file, rank, ans):
        assert self.s.get_color_as_key(_file, rank) == ans

#!/usr/bin/env python3
"""
Practice which color a square is light or dark
"""

from say_squares import SaySquare

s = SaySquare(
    config={
        # map keys to rank and files
        # 0 = file 1 and rank a
        # 1 is file 2 and rank b
        # and so on
        "keymap": {
            "a": 0,
            "b": 1,
            "c": 2,
            "d": 3,
            "e": 4,
            "f": 5,
            "g": 6,
            "h": 7,
            "1": 0,
            "2": 1,
            "3": 2,
            "4": 3,
            "5": 4,
            "6": 5,
            "7": 6,
            "8": 7,
        },
        # used for light square dark square
        "light_key": ";",
        "dark_key": "'",
    }
)

s.which_color(100)

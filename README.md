a collection of python scripts for training blind chess written in
python. Features include:

- quizzing whether square is light or dark.
- quizzing for valid moves for knights and bishop
- reading games out loud for visualization 
- recording of answers 

# Quickstart

install dependencies
```
sudo apt install libespeak1

pip install -r requirements.txt
```

run
```
python say_squares.py
```
